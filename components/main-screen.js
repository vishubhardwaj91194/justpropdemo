import React, {Component} from 'React';
import {
	View,
	Text,
	Image,
	BackAndroid,
	Alert
} from 'react-native';
import CustomStatus from './status-bar';
import MainToolbar from './main-toolbar';
import Drawer from 'react-native-drawer'
import MainSideMenu from './main-side-menu';
import mainScreenStyle from './../stylesheet/main-style';

BackAndroid.addEventListener('hardwareBackPress', () => {
	//Alert.alert("ffff");
	if (drawer.isop) {
		drawer.draw.close();
		drawer.isop = false;
		return true;
	} else {
		return false;
	}
});


var drawer = {draw:null, isop: false};

class MainScreen extends Component {

  closeControlPanel = () => {
  	drawer.isop = false;
    this._drawer.close()
  };
  openControlPanel = () => {
  	drawer.isop = true;
    this._drawer.open()
  };

	render () {
		return (
			<Drawer
				ref={(ref) => {
					this._drawer = ref;
					drawer.draw = this._drawer;
				}}
    			content={<MainSideMenu />}
    			type="overlay"
    			openDrawerOffset={.25}
    			panOpenMask={.2}
    			panCloseMask={.9}
    			style={mainScreenStyle.drawer}
    			captureGestures={false}
    			negotiatePan={true}
    			tweenHandler={
    				ratio => ({
						main: {
							opacity: 1,
						},
						mainOverlay: {
							opacity: ratio / 2,
							backgroundColor: 'black',
						},
					})
    			}
    			onOpenStart={()=>{drawer.isop=true;}}
    			onCloseStart={()=>{drawer.isop=false;}}
    		>
				<View>
					<CustomStatus />
					<MainToolbar openMenu={this.openControlPanel} />
					<Text>tess</Text>
				</View>
			</Drawer>
		);
	}



}

export default MainScreen;