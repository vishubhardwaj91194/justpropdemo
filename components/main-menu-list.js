import React, {Component} from 'React';
import {
	View,
	Text
} from 'react-native';
import menuListStyle from './../stylesheet/menu-list-style';

class MainMenuList extends Component {

	static defaultProps = {
		header: 'Header',
		list: ['list 1', 'list 2', 'list 3'],
		last: false
	}
	
	render () {
		const size = this.props.list.length-1;
		const last = this.props.last

		return (
			<View>
				<View style={menuListStyle.header}>
					<Text style={menuListStyle.headerText}>{this.props.header.toUpperCase()}</Text>
				</View>
				{
					this.props.list.map(function(listItem, index) {
						return (
							<View style={
								(last)
									?(menuListStyle.listItem)
									:(index==size)
										?menuListStyle.listNoBorder
										:menuListStyle.listItem
							}>
								<Text
									style={menuListStyle.listItemText}
								>
									{capitalize(listItem)}
								</Text>
							</View>
						);
					})
				}
			</View>
		);
	}
}

const capitalize = (str) => {
		var spc = true;
		var b = '';
		for (var i=0; i<str.length; i++) {
			var d = str.charAt(i).toString();
			if (spc == true) {
				b = b.concat(
					d.toUpperCase()
				);
			} else {
				b = b.concat(
					d.toLowerCase()
				);
			}

			if (d.startsWith(" ")) {
				spc = true;
			} else {
				spc = false;
			}
		}
		return b;
	}

export default MainMenuList;