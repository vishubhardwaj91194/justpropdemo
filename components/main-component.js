import React, {Component} from 'React';
import {
	View,
	Text,
	Navigator
} from 'react-native';
import HelloWorld from './hello-world-component';
import SplashScreen from './splash-screen';
import MainScreen from './main-screen';

class MainComponent extends Component {

	render () {
		return (
			<Navigator
				initialRoute={{pageTitle: 'Splash screen', pageId:0}}
				renderScene={this.navigatorRenderScene}
			/>
		);
	}

	navigatorRenderScene(route, navigator) {
		switch (route.pageId) {
			case 0:
				return <SplashScreen navigator={navigator} />;
			case 1:
				return <MainScreen navigator={navigator} />;
			default:
				return (<HelloWorld />);
		}
	}
}

export default MainComponent;