import React, {Component} from 'React';
import {View, Text} from 'react-native';
import CustomStatus from './status-bar';

class HelloWorld extends Component {
	render () {
		return (
			<View>
				<CustomStatus />
				<Text>Hello World</Text>
			</View>
		);
	}
}

export default HelloWorld;