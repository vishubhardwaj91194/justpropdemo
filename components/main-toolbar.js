import React, {Component} from 'React';
import {
	View,
	Image,
	TouchableOpacity
} from 'react-native';
import toolbarStyle from './../stylesheet/main-toolbar-style';

class MainToolbar extends Component {
	render () {
		return (
			<View style={toolbarStyle.container}>
				<View style={toolbarStyle.row}>
					<TouchableOpacity
						onPress={this.props.openMenu}
					>
						<Image
							source={require('./../images/hamburger.png')}
							style={toolbarStyle.hamburger}
						/>
					</TouchableOpacity>
					<Image
						source={require('./../images/toolbar_logo.png')}
						style={toolbarStyle.logo}
					/>
					<Image
						source={require('./../images/btn_notifications_off.png')}
						style={toolbarStyle.notification}
					/>
				</View>
			</View>
		);
	}
}

export default MainToolbar;