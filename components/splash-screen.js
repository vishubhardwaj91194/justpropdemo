import React, {Component} from 'React';
import {
	View,
	Text,
	StatusBar,
	Image
} from 'react-native';
import splashStyles from './../stylesheet/splash-screen-style'
import CustomStatus from './status-bar';

class SplashScreen extends Component {

	constructor (props) {
		super(props);
		setTimeout(
			() => {
				this.props.navigator.replace({
					pageTitle: 'Main screen',
					pageId: 1
				});
			}, 2000
		);
	}

	render () {
		return (
			<View>
				<CustomStatus hidden={true} />
				<Image
					source={require('./../images/splash_screen.png')}
					style={splashStyles.image}
				/>
			</View>
		);
	}
}

export default SplashScreen;