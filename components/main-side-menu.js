import React, {Component} from 'React';
import {
	View,
	Image,
	Text,
	ScrollView
} from 'react-native';
import sideMenuStyle from './../stylesheet/side-menu-style';
import MainMenuList from './main-menu-list.js';

class MainSideMenu extends Component {
	render () {
		return (
			<View style={sideMenuStyle.container}>
				<View style={sideMenuStyle.header}>

					<Image
						source={require('./../images/logo_menu.png')}
						style={sideMenuStyle.logo}
					/>

				</View>

				<ScrollView elevation={3} zindex={3}>

					<View style={sideMenuStyle.profile}>
						<View style={sideMenuStyle.profRow}>
						<Image
							source={require('./../images/icn_user_default.png')}
							style={sideMenuStyle.profIcon}
						/>
						<Text style={sideMenuStyle.profName}>shubhang m</Text>
						</View>
					</View>
					<MainMenuList header="profile" list={
						['saved searches', 'notification settings', 'sign out']
					} />
					<MainMenuList header="residential" list={
						['to rent', 'to buy']
					} />
					<MainMenuList header="comercial property" list={
						['to rent', 'to buy']
					} />
					<MainMenuList header="more" list={
						['Short-Term property rental', 'set country']
					} last={true} />

				</ScrollView>
			</View>
		);
	}
}

export default MainSideMenu;