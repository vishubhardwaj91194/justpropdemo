import React, {Component} from 'React';
import {
	View,
	StatusBar
} from 'react-native';

class CustomStatus extends Component {

	static defaultProps = {
		hidden: false
	}

	render () {
		return (
			<StatusBar
				backgroundColor="#09BAC2"
				barStyle="light-content"
				hidden={this.props.hidden}
			/>
		);
	}
}

export default CustomStatus;