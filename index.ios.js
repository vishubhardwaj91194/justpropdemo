/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import MainComponent from './components/main-component';

export default class JustProp extends Component {
  render() {
    return (
      <MainComponent />
    );
  }
}


AppRegistry.registerComponent('JustProp', () => JustProp);
