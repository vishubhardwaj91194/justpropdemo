import {StyleSheet, Dimensions} from 'react-native';

// Store width in variable
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

const toolbarStyle = StyleSheet.create({
	container: {
		width: width,
		height: 40,
		backgroundColor: '#09BAC2'
	},
	row: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between'
	},
	hamburger: {
		height: 20,
		width: 20,
		marginLeft: 20,
	},
	logo: {
		height: 20,
		width: 200,
		resizeMode: 'contain'
	},
	notification: {
		height: 20,
		width: 20,
		marginRight: 20
	}
});

export default toolbarStyle;