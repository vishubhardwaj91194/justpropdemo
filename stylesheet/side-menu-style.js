import {StyleSheet} from 'react-native';

const sideMenuStyle = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#ffffff',
		width:300
	},
	header: {
		height: 100,
		justifyContent: 'center',
		alignItems: 'center',
	},
	logo: {
		height: 25,
		resizeMode: 'contain'
	},
	profile: {
		height: 40,
		backgroundColor: '#ffbf00'
	},
	profIcon: {
		height: 30,
		width: 30,
		resizeMode: 'contain'
	},
	profName: {
		color: '#ffffff',
		fontSize: 18,
		paddingLeft: 20
	},
	profRow: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		paddingLeft: 30
	}
});

export default sideMenuStyle;