import {StyleSheet} from 'react-native';

const menuListStyle = StyleSheet.create({
	header: {
		backgroundColor: '#e6e6e6',
		height: 25,
		justifyContent: 'center'
	},
	headerText: {
		fontSize: 14,
		marginLeft: 30
	},
	listItem: {
		height: 40,
		justifyContent: 'center',
		borderBottomWidth: 1,
		borderBottomColor: '#999999',
		marginLeft: 30
	},
	listItemText: {

	},
	listNoBorder: {
		height: 40,
		justifyContent: 'center',
		marginLeft: 30
	}
});

export default menuListStyle;