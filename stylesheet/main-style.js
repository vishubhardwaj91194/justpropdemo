import {StyleSheet} from 'react-native';

const mainScreenStyle = StyleSheet.create({
	drawer: {
		shadowColor: '#000000',
		shadowOpacity: 0,
		shadowRadius: 5,
	}
});

export default mainScreenStyle;