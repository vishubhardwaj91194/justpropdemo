import {StyleSheet, Dimensions} from 'react-native';

// Store width in variable
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

const splashStyles = StyleSheet.create({
	image: {
		width: width,
		height: height
	}
});

export default splashStyles;